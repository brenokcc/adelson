# -*- coding: utf-8 -*-
import datetime
from djangoplus.db import models
from djangoplus.decorators import attr, action, subset
#from multiupload.fields import MultiMediaField


class Estado(models.Model):
    nome = models.CharField(verbose_name=u'Nome', search=True)
    sigla = models.CharField(verbose_name=u'Sigla', search=True)

    fieldsets = (
        (u'Dados Gerais', {'fields': ('nome', 'sigla')}),
        (u'Cadastros::Municípios', {'relations': ('municipio_set',),}),
    )

    class Meta:
        ordering = ['nome']
        verbose_name = u'Estado'
        verbose_name_plural = u'Estados'
        domain = u'Cadastros Básicos', 'fa-th'
        log = True

    def __unicode__(self):
        return self.nome


class Municipio(models.Model):
    nome = models.CharField(verbose_name=u'Nome', search=True)
    estado = models.ForeignKey('Estado', verbose_name=u'Estado')

    class Meta:
        ordering = ['nome']
        verbose_name = u'Município'
        verbose_name_plural = u'Municípios'
        domain = u'Cadastros Básicos', 'fa-th'
        log = True

    def __unicode__(self):
        return u'%s/%s' % (self.nome, self.estado)


class Sexo(models.Model):
    nome = models.CharField(verbose_name=u'Nome', search=True)

    def __unicode__(self):
        return self.nome

    class Meta:
        ordering = ['nome']
        verbose_name = u'Sexo'
        verbose_name_plural = u'Sexos'
        domain = u'Cadastros Básicos', 'fa-th'
        log = True


class Porte(models.Model):
    nome = models.CharField(verbose_name=u'Nome', search=True)

    fieldsets = (
        (u'Dados Gerais', {'fields': ('nome', )}),
        (u'Estatísica::Gráficos', {'extra': ('get_quantidade',)}),
    )

    class Meta:
        ordering = ['nome']
        verbose_name = u'Porte'
        verbose_name_plural = u'Portes'
        domain = u'Cadastros Básicos', 'fa-th'
        log = True

    def __unicode__(self):
        return self.nome

    @attr(u'Quantidade de Animais', template_filter='chart')
    def get_quantidade(self):
        return Animal.objects.filter(porte=self).count('porte')


class Especie(models.Model):
    nome = models.CharField(u'Nome', null=False, blank=False, search=True)

    fieldsets = (
        (u'Dados Gerais', {'fields': ('nome',)}),
        (u'Cadastros::Raças', {'relations': ('raca_set',),}),
        (u'Estatísica::Gráficos', {'extra': ('get_quantidade',)}),
    )

    class Meta:
        verbose_name = u'Espécie'
        verbose_name_plural = u'Espécies'
        domain = u'Cadastros Básicos', 'fa-th'
        can_admin = u'Administrador'
        list_display = 'nome',
        log = True

    def __unicode__(self):
        return self.nome

    @attr(u'Quantidade de Animais', template_filter='chart')
    def get_quantidade(self):
        print PessoaFisica.objects.filter(cpf=self.request.user.username)
        return Animal.objects.filter(raca__especie=self).count('raca')


class Raca(models.Model):
    nome = models.CharField(u'Nome', null=False, blank=False, search=True)
    especie = models.ForeignKey(Especie, verbose_name=u'Espécie')

    fieldsets = (
        (u'Dados Gerais', {'fields': ('especie', 'nome',)}),
    )

    class Meta:
        verbose_name = u'Raça'
        verbose_name_plural = u'Raças'
        domain = u'Cadastros Básicos', 'fa-th'
        can_admin = u'Administrador'
        log = True

    def __unicode__(self):
        return self.nome


class PessoaFisica(models.Model):
    nome = models.CharField(verbose_name=u'Nome', search=True)
    cpf = models.CpfField(verbose_name=u'CPF', search=True)
    sexo = models.ForeignKey(Sexo, verbose_name=u'Sexo', null=True, blank=True, search=True)
    telefone = models.PhoneField('Telefone', blank=True, default='')
    email = models.CharField(verbose_name=u'E-mail', blank=True, default='')

    #endereco
    logradouro = models.CharField(verbose_name=u'Logradouro', blank=True)
    numero = models.CharField(verbose_name=u'Número', blank=True)
    complemento = models.CharField(verbose_name=u'Complemento', blank=True)
    bairro = models.CharField(verbose_name=u'Bairro', blank=True)
    cep = models.CharField(verbose_name=u'CEP', blank=True)
    municipio = models.ForeignKey(Municipio, verbose_name=u'Município', null=True, blank=True)
    ponto_referencia = models.TextField(verbose_name=u'Ponto de referência', blank=True)

    fieldsets = (
        (u'Dados Gerais', {'fields': ('nome', ('cpf', 'sexo'), ('email', 'telefone'),)}),
        (u'Endereço', {'fields': (('logradouro', 'numero'), ('bairro', 'cep'), ('complemento', 'municipio'), 'ponto_referencia')}),
    )

    class Meta:
        ordering = ['nome']
        verbose_name = u'Pessoa Física'
        verbose_name_plural = u'Pessoas Físicas'
        can_admin = 'Administrador'
        domain = u'Usuários', 'fa-user-plus'
        add_shortcut = True
        list_shortcut = True
        icon = 'fa-user'
        verbose_female = True
        role_name = 'nome'
        role_username = 'email'
        role_signup = True
        log = True

    def __unicode__(self):
        return self.nome


class Administrador(models.Model):
    pessoa_fisica = models.ForeignKey(PessoaFisica, verbose_name=u'Pessoa Física')

    fieldsets = (
        (u'Dados Gerais', {'fields': ('pessoa_fisica',)}),
    )

    class Meta:
        verbose_name = u'Administrador'
        verbose_name_plural = u'Administradores'
        domain = u'Usuários', 'fa-user-plus'
        role_username = 'pessoa_fisica__cpf'
        log = True

    def __unicode__(self):
        return self.pessoa_fisica.nome


class Voluntario(models.Model):
    pessoa_fisica = models.ForeignKey(PessoaFisica, verbose_name=u'Pessoa Física')

    fieldsets = (
        (u'Dados Gerais', {'fields': ('pessoa_fisica',)}),
    )

    class Meta:
        verbose_name = u'Voluntário'
        verbose_name_plural = u'Voluntários'
        domain = u'Usuários', 'fa-user-plus'
        role_username = 'pessoa_fisica__cpf'
        log = True

    def __unicode__(self):
        return self.pessoa_fisica.nome


class AnimalManager(models.Manager):

    @subset(u'Necessitando Adoção', alert=True)
    def necessitando_adocao(self):
        return self.filter(adotante__isnull=True)


class Animal(models.Model):
    nome = models.CharField(u'Nome', null=False, blank=False, search=True)
    sexo = models.ForeignKey(Sexo, verbose_name=u'Sexo', lazy=True, filter=True)
    raca = models.ForeignKey(Raca, verbose_name=u'Raça', lazy=True, filter=True)
    nascimento = models.DateField(verbose_name=u'Nascimento', filter=True)
    responsavel = models.ForeignKey('PessoaFisica', verbose_name=u'Responsável', help_text='Pessoa responsável pelo animal', related_name='animais', search=True)
    castrado = models.NullBooleanField(verbose_name=u'Castrado', search=True)
    vacinado = models.NullBooleanField(verbose_name=u'Vacinado', search=True)
    porte = models.ForeignKey('Porte', verbose_name=u'Porte', null=True, blank=True, search=True)
    necessidade_especial = models.TextField(verbose_name=u'Necessidade Especial', blank=True, search=True)
    comportamento = models.TextField(verbose_name=u'Comportamento', blank=True, search=True)
    observacoes = models.TextField(verbose_name=u'Observações', blank=True, search=True)
    cor_predominante = models.CharField(verbose_name=u'Cor Predominante', blank=True, search=True)
    foto = models.ImageField(upload_to='objetos', null=True, blank=True)
    # fotos = MultiImageField(min_num=1, max_num=3, max_file_size=1024*1024*5)
    # fotos = MultiMediaField(
    #     min_num=1,
    #     max_num=3,
    #     max_file_size=1024*1024*5,
    #     media_type= 'image'#'video', 'audio', 'video' or 'image'
    # )

    adotante = models.ForeignKey(PessoaFisica, verbose_name=u'Adotante', null=True, exclude=True, related_name='adotante_set')
    data_adocao = models.DateField(verbose_name=u'Data da Adoção', filter=True, null=True, exclude=True)
    fieldsets = (
        (u'Dados Gerais', {'fields': ('nome',
                                      ('sexo', 'castrado', 'vacinado'),
                                      ('raca', 'porte'),
                                      ('nascimento', 'responsavel'),
                                      ('necessidade_especial', 'cor_predominante'),
                                      'foto',
                                      'comportamento',
                                      'observacoes')}),
    )

    objects = AnimalManager()

    class Meta:
        verbose_name = u'Animal'
        verbose_name_plural = u'Animais'
        icon = 'fa-paw'
        can_admin = 'Administrador', 'Voluntário'
        log = True

    def __unicode__(self):
        return self.nome

    # def pode_colocar_adocao(self):
    #     return self.resposta and not self.resposta_final
    #
    # @action(u'Colocar para Adoção', perm_or_group=u'Voluntário, Administrador', condition='pode_colocar_adocao', style='popup btn-success')
    # def registrar_adocao(self):
    #     self.adotante = adotante
    #     self.data_adocao = data_adocao
    #     self.save()
    #
    # def registrar_adocao_choices(self):
    #     adotantes = PessoaFisica.objects.exclude(pk=self.responsavel.pk)
    #     return dict(adotante=adotantes)
    #
    # def registrar_adocao_initial(self):
    #     return dict(data_adocao=datetime.date.today())
    #
    # @action(u'Cancelar Adoção', perm_or_group=u'Voluntário', condition='adotante', style='ajax btn-danger')
    # def cancelar_adocao(self):
    #     self.adotante = None
    #     self.data_adocao = None
    #     self.save()


class AnimalAdotado(models.Model):
    responsavel = models.ForeignKey('PessoaFisica', help_text=u'Pessoa atualmente responsável pelo animal', related_name='responsavel_animais_adotados')
    adotante = models.ForeignKey('PessoaFisica', related_name='adocoes_feitas', null=True, blank=True)
    nome_animal = models.CharField(u'Nome do Animal', search=True, blank=True, help_text=u'Nome dado pelo adotante')
    animal = models.ForeignKey('Animal')
    aprovado = models.NullBooleanField(help_text=u'Tem que ser aprovado para publicação')
    aprovador = models.ForeignKey('PessoaFisica', help_text=u'Pessoa que aprovou a publicacao', related_name='aprovador_animais_adotados', null=True, blank=True)
    data = models.DateField(auto_now_add=True)

    class Meta:
        verbose_name = u'Animal para adoção'
        verbose_name_plural = u'Animais para adoção'
        icon = 'fa-paw'
        can_admin = u'Administrador'
        log = True

    def __unicode__(self):
        return u'Animal para Adoção %s, responsável %s' % (self.animal, self.responsavel)


class AnimalLT(models.Model):
    responsavel = models.ForeignKey('PessoaFisica', help_text=u'Pessoa atualmente responsável pelo animal', related_name='responsavel_animais_tl')
    nome_animal = models.CharField(u'Nome do Animal', null=False, blank=False, search=True)
    animal = models.ForeignKey('Animal')
    aprovado = models.NullBooleanField(help_text=u'Tem que ser aprovado para publicação')
    aprovador = models.ForeignKey('PessoaFisica', help_text=u'Pessoa que aprovou a publicacao', related_name='aprovador_animais_lt', null=True, blank=True)
    data = models.DateField(auto_now_add=True)

    class Meta:
        verbose_name = u'Animal para Lar Temporário'
        verbose_name_plural = u'Animais para Lar Temporário'
        icon = 'fa-paw'
        can_admin = u'Administrador'
        log = True

    def __unicode__(self):
        return u'Animal para LT %s, responsável %s' % (self.animal, self.responsavel)



class AnimalRegatado(models.Model):
    responsavel = models.ForeignKey('PessoaFisica', help_text=u'Pessoa atualmente responsável pelo animal', related_name='responsavel_animais_resgatados')
    nome_animal = models.CharField(u'Nome do Animal', null=False, blank=False, search=True)
    animal = models.ForeignKey('Animal')
    aprovado = models.NullBooleanField(help_text=u'Tem que ser aprovado para publicação')
    aprovador = models.ForeignKey('PessoaFisica', help_text=u'Pessoa que aprovou a publicacao', related_name='aprovador_animais_resgatados', null=True, blank=True)
    data = models.DateField(auto_now_add=True)

    class Meta:
        verbose_name = u'Animal Regatado'
        verbose_name_plural = u'Animais Regatados'
        icon = 'fa-paw'
        can_admin = u'Administrador'
        log = True

    def __unicode__(self):
        return u'Animal Resgatado %s, responsável %s' % (self.animal, self.responsavel)