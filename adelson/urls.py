# -*- coding: utf-8 -*-
from django.conf.urls import url, include
from adelson import views
from djangoplus.admin.views import index

urlpatterns = [
    url(r'^$', views.landing),
    url(r'^admin/$', index),
    url(r'', include('djangoplus.admin.urls')),
]

