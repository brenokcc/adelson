# -*- coding: utf-8 -*-
from djangoplus.conf.base_settings import *
from os.path import abspath, dirname, join, exists
from os import sep

BASE_DIR = abspath(dirname(dirname(__file__)))
PROJECT_NAME = __file__.split(sep)[-2]

STATIC_ROOT = join(BASE_DIR, 'static')
MEDIA_ROOT = join(BASE_DIR, 'media')

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': join(BASE_DIR, 'sqlite.db'),
        'USER': '',
        'PASSWORD': '',
        'HOST': '',
        'PORT': '',
    }
}

WSGI_APPLICATION = '%s.wsgi.application' % PROJECT_NAME

INSTALLED_APPS += (
    PROJECT_NAME,
)

ROOT_URLCONF = '%s.urls' % PROJECT_NAME

if exists(join(BASE_DIR, 'logs')):
    DEBUG = False
    ALLOWED_HOSTS = ['*']
else:
    EMAIL_BACKEND = 'django.core.mail.backends.filebased.EmailBackend'
    EMAIL_FILE_PATH = join(BASE_DIR, 'mail')

DROPBOX_TOKEN = '' #https://www.dropbox.com/developers/apps
BACKUP_FILES = ['media', 'sqlite.db']
TEMPLATES[0]['OPTIONS']['builtins'].append('%s.templatefilters' % PROJECT_NAME)
